This is a wrapper around [skopeo](https://github.com/containers/skopeo) to help migrate images between two container registries. Skopeo doesn't require docker or podman to be installed!

## How to use

`image-migrator.sh` expects several variables (which can be set in `config.sh` file):

* `SOURCE_USER` - username on source for docker login
* `SOURCE_PAT` - password / token on source for docker login
* `TARGET_USER` - username on target for docker login
* `TARGET_PAT` - password / token on target for docker login
* `BASE_URL_SOURCE` - base URL on source (i.e. *registry.gitlab.com/erudinsky/source*)
* `BASE_URL_TARGET` - base URL on target (i.e. *registry.gitlab.com/evgenyrudinsky/target*)

`images.yml` contains list of images on source to be migrated (one image per line), example:

```yaml

registry.gitlab.com/erudinsky/source:1
registry.gitlab.com/erudinsky/source/myapp:1

```

Once done: 

```bash 

chmod +x image-migrator.sh

./image-migrator.sh

```

## Contacts

Tag @erudinsky for anything related to this tool.